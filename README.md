# dark_themer

Theme generator for BaseRow via DarkReader's Dynamic CSS exports and future other opensource productivity tools that needs dark theme.

## Getting started

- install darkreader chrome extension (opensource)
- Visit exsting baserow and save generated css
- `pip install tinycss2`
- `mkdir baserow_dark`
- `cd baserow_dark`
- `git clone https://gitlab.com/bramw/baserow.git`
- `git clone https://gitlab.com/v3ss0n/dark_themer.git`
- `cd dark_themer`
- `python dark_themer.py`
- `cd ../baserow`
- `./dev.sh restart`

Tested against 1,11
## License

MIT

## Project status

Active
