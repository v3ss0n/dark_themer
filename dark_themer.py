import tinycss2


def token_str(token):
    return "".join(map(lambda x: x.value, token))


def parse_color_properties(tokens):
    asts = tinycss2.parse_declaration_list(tokens, skip_comments=True, skip_whitespace=True)
    results = []
    for prop in asts:
        value = "".join([t.serialize() for t in prop.value]).strip()
        if prop.lower_name in ["color", "background-color"]:
            return value


def color_replacer(default_rules, dark_rules, replace_sass):
    default_rules_map = {}
    replacer = {}
    for c in default_rules:
        try:
            d = parse_color_properties(c.content)
            default_rules_map[token_str(c.prelude)] = d
        except:
            # print("Error",c.prelude)
            pass

    for rules in dark_rules:
        try:
            d = parse_color_properties(rules.content)
            prelude = token_str(c.prelude)
        except:
            # print("Error",c.prelude)
            pass
        for k, v in default_rules_map.items():
            if prelude == k:
                replacer[default_rules_map[k]] = d

    # replace_map =
    default_vars = replace_sass.readlines()
    replaced = []
    for ln in default_vars:
        for k, v in replacer.items():
            if ln.startswith(k):
                ln = f"{k}: {v} !default; //dark_themer \n"
        replaced.append(ln)
    # print (replaced)
    return "".join(replaced)


def dark_themer(
    input_vars="../baserow/web-frontend/modules/core/assets/scss/variables.scss",
    colors_rules="../baserow/web-frontend/modules/core/assets/scss/components/colors.scss",
    darkreader_css="./darkreader_theme.css",
    output_file="../baserow/web-frontend/modules/core/assets/scss/variables.scss",
):
    sass_vars = open(input_vars, "r")
    sasscolors = open(colors_rules, "r").read()
    dark_colors = open(darkreader_css, "r").read()
    color_rules = tinycss2.parse_stylesheet(sasscolors, skip_comments=True, skip_whitespace=True)
    dark_rules = tinycss2.parse_stylesheet(dark_colors, skip_comments=True, skip_whitespace=True)

    dark_themed = color_replacer(color_rules, dark_rules, sass_vars)
    if output_file:
        with open(output_file, "w") as themed:
            themed.write(dark_themed)
    else:
        print(dark_themed)


if __name__ == "__main__":
    dark_themer()
    # dark_themer(output_file=None)
